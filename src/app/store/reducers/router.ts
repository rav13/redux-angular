import { ActivatedRouteSnapshot, Params, RouterStateSnapshot } from '@angular/router';
import * as routerNgrx from '@ngrx/router-store';
import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

export interface RouterStateUrl {
  url: string;
  queryParams: Params;
  params: Params;
}

export interface State {
  routerReducer: routerNgrx.RouterReducerState<RouterStateUrl>;
}

export const reducers: ActionReducerMap<State> = {
  routerReducer: routerNgrx.routerReducer,
};

export const getRouterState = createFeatureSelector<
  routerNgrx.RouterReducerState<RouterStateUrl>
>('routerReducer');

export class CustomSerializer
  implements routerNgrx.RouterStateSerializer<RouterStateUrl> {
  serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    const { url } = routerState;

    const { queryParams } = routerState.root;

    let state: ActivatedRouteSnapshot = routerState.root;

    while (state.firstChild) {
      state = state.firstChild;
    }
    const { params } = state;
    return { url, queryParams, params };
  }
}
