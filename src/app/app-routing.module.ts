import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
    ProductDetailComponent,
} from './products/components/product-detail/product-detail.component';
import { ProductsHomeComponent } from './products/components/products-home/products-home.component';

const routes: Routes = [
  {
    path: 'products',
    component: ProductsHomeComponent,
  },
  {
    path: 'products/:id',
    component: ProductDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
