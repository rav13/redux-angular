export interface Car {
  id: number;
  name: string;
  description: string;
  image: string;
}
