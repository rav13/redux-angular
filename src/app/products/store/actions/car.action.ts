import { Action } from '@ngrx/store';

import { Car } from '../../interface/car';

export const LOAD_CAR = '[Car] Load Car';
export const UPDATE_CAR = '[Car] Update Car';
export const LOAD_CAR_FAIL = '[Car] Load Car Fail';
export const LOAD_CAR_SUCCESS = '[Car] Load Car Success';
export const DELETE_CAR = '[Car] Delete Car';

export class LoadCar implements Action {
  readonly type = LOAD_CAR;
}

export class LoadCarFail implements Action {
  readonly type = LOAD_CAR_FAIL;
  constructor(public car: Car) {}
}

export class LoadCarSuccess implements Action {
  readonly type = LOAD_CAR_SUCCESS;
  constructor(public car: Car[]) {}
}

export class UpdateCar implements Action {
  readonly type = UPDATE_CAR;
  constructor(public car: Car) {}
}

export class DeleteCar implements Action {
  readonly type = DELETE_CAR;
  constructor(public car: Car) {}
}

export type CarAction =
  | LoadCar
  | LoadCarFail
  | LoadCarSuccess
  | UpdateCar
  | DeleteCar;
