import { getRouterState } from 'src/app/store/reducers/router';

import { RouterReducerState } from '@ngrx/router-store';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

import { RouterStateUrl } from '../../../store/reducers/router';
import { Car } from '../../interface/car';
import { carReducer, CarState, getCarLoaded, getCarLoading, getCars } from './car.reducer';

export interface ProductsState {
  cars: CarState;
}

export const reducers: ActionReducerMap<ProductsState> = {
  cars: carReducer,
};

export const getProductsState = createFeatureSelector<ProductsState>(
  'products'
);

export const getCarState = createSelector(
  getProductsState,
  (productState: ProductsState) => productState.cars
);

export const getAllCars = createSelector(getCarState, getCars);

export const getCarsLoaded = createSelector(getCarState, getCarLoaded);

export const getCarsLoading = createSelector(getCarState, getCarLoading);

export const getSelectedCar = createSelector(
  getCarState,
  getRouterState,
  (state: CarState, router: RouterReducerState<RouterStateUrl>): Car => {
    const car = state.data.find(
      (car) => car.id === Number(router.state.params.id)
    );
    return car;
  }
);
