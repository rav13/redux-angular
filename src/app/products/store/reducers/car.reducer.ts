import { Car } from '../../interface/car';
import * as actions from '../actions/car.action';

export interface CarState {
  data: Car[];
  loaded: boolean;
  loading: boolean;
}

export const initialState: CarState = {
  data: [],
  loaded: false,
  loading: false,
};

export const carReducer = (
  state: CarState = initialState,
  action: actions.CarAction
) => {
  switch (action.type) {
    case actions.LOAD_CAR: {
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    }

    case actions.LOAD_CAR_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false,
      };
    }

    case actions.LOAD_CAR_SUCCESS: {
      const data = action.car;
      return {
        ...state,
        loading: false,
        loaded: true,
        data,
      };
    }

    case actions.UPDATE_CAR: {
      console.log(action.car);
      const data = state.data.map((p) => {
        if (p.id === action.car.id) {
          return { ...p, ...action.car };
        } else {
          return p;
        }
      });
      return {
        ...state,
        loading: false,
        loaded: false,
        data,
      };
    }

    case actions.DELETE_CAR: {
      console.log(action.car);
      const data = state.data.filter((p) => p.id !== action.car.id);
      return {
        data,
        loading: false,
        loaded: false,
      };
    }

    default:
      return state;
  }
};

export const getCarLoading = (carState: CarState) => carState.loading;
export const getCarLoaded = (carState: CarState) => carState.loaded;
export const getCars = (carState: CarState) => carState.data;
