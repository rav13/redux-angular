import { map, switchMap } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { CarService } from '../../service/car.service';
import { LOAD_CAR, LoadCarSuccess } from '../actions/car.action';

@Injectable()
export class CarEffect {
  constructor(private action$: Actions, private service: CarService) {}

  @Effect()
  loadCars$ = this.action$.pipe(
    ofType(LOAD_CAR),
    switchMap(() => {
      return this.service
        .getCars()
        .pipe(map((cars) => new LoadCarSuccess(cars)));
    })
  );
}
