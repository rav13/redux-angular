import { Observable } from 'rxjs';

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { Car } from '../../interface/car';
import { UPDATE_CAR, UpdateCar } from '../../store/actions/car.action';
import { getSelectedCar, ProductsState } from '../../store/reducers/reducer';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css'],
})
export class ProductDetailComponent implements OnInit {
  car$: Observable<Car>;
  car: Car;
  newDescription: string = '';
  constructor(private store: Store<ProductsState>, private router: Router) {}

  ngOnInit() {
    console.log('inicia');
    this.car$ = this.store.select(getSelectedCar);
    this.car$.subscribe((car) => (this.car = car));
    console.log(this.car);
  }

  edit() {
    const car: Car = {
      id: this.car.id,
      name: this.car.name,
      description: this.newDescription,
      image: this.car.image,
    };
    this.store.dispatch(new UpdateCar(car));
    this.router.navigate(['products']);
  }
}
