import { Observable } from 'rxjs';

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { Car } from '../../interface/car';
import { DeleteCar, LoadCar } from '../../store/actions/car.action';
import { getAllCars, ProductsState } from '../../store/reducers/reducer';

@Component({
  selector: 'app-products-home',
  templateUrl: './products-home.component.html',
  styleUrls: ['./products-home.component.css'],
})
export class ProductsHomeComponent implements OnInit {
  cars$: Observable<Car[]>;

  constructor(private store: Store<ProductsState>, private router: Router) {}

  ngOnInit(): void {
    if (localStorage.getItem('iniciado') === '0') {
      this.store.dispatch(new LoadCar());
    }
    localStorage.setItem('iniciado', '1');
    this.cars$ = this.store.select(getAllCars);
  }

  goToProductView(id: string) {
    this.router.navigate([`products/${id}`]);
  }

  deleteCar(car: Car) {
    this.store.dispatch(new DeleteCar(car));
  }
}
