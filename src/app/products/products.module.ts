import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { ProductDetailComponent } from './components/product-detail/product-detail.component';
import { ProductsHomeComponent } from './components/products-home/products-home.component';
import { effects } from './store/effects/effects';
import { reducers } from './store/reducers/reducer';

@NgModule({
  declarations: [ProductsHomeComponent, ProductDetailComponent],
  imports: [
    CommonModule,
    StoreModule.forFeature('products', reducers),
    EffectsModule.forFeature(effects),
    HttpClientModule,
    FormsModule,
  ],
  exports: [ProductsHomeComponent],
})
export class ProductsModule {}
